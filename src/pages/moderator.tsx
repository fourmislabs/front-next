import { GetStaticProps } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import ClientOnly from "@/components/ClientOnly";
import { RequireModeratorRole } from "@/components/guard";

const Moderator = (props: any) => {
  return (
    <>
      <ClientOnly>
        <RequireModeratorRole>
          <div>moderator page</div>
        </RequireModeratorRole>
      </ClientOnly>
    </>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  return {
    props: {
      ...(await serverSideTranslations(context.locale ?? "fr", [
        "menu",
        "foot",
        "auth-page",
        "banner",
        "foot"
      ])),
    },
  };
};

export default Moderator;
