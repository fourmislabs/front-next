import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { GetServerSideProps } from "next";
import { getCookies } from "cookies-next";
import PageNotFound from "./404";

const Home = ({ roles }: { roles: string[] }) => {
  return <>{roles === null ? <PageNotFound /> : <div>Homepage</div>}</>;
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { req, locale } = context;
  const Cookie = JSON.parse(JSON.stringify(getCookies({ req })));
  let roles: string[] | null = null;
  if (Cookie.roles) roles = JSON.parse(Cookie.roles);
  return {
    props: {
      ...(await serverSideTranslations(locale ?? "fr", ["menu", "common", "banner", "foot"])),
      roles,
    },
  };
};

export default Home;
