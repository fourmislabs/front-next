import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { GetStaticProps } from "next";
import General from "@/components/profile/sections/General";
import style from "../styles/pages/profile-page.module.scss";
import { Container, Row, Col } from "react-grid-system";
import { Avatar } from "@chakra-ui/react";
import { useUserStore } from "@/stores/userStore";
import { useTranslation } from "next-i18next";
import ClientOnly from "@/components/ClientOnly";
import dynamic from "next/dynamic";
import { RequireAuth } from "@/components/guard";
import { FC } from "react";

import { useRouter } from "next/router";

const Footer = dynamic(() => import("../components/footer/Footer"), {
  loading: () => <></>,
});

const Social = dynamic(() => import("../components/profile/sections/Social"));
const Security = dynamic(
  () => import("../components/profile/sections/Security")
);

const Notifications = dynamic(
  () => import("../components/profile/sections/Notifications")
);
const Inbox = dynamic(() => import("../components/profile/sections/Inbox"));

const ProfilePage: FC = (props: any) => {
  const { t } = useTranslation(["profile-page"]);
  const router = useRouter();

  const [avatar, username, isAdmin, isModerator] = useUserStore((state) => [
    state.avatar,
    state.username,
    state.isAdmin,
    state.isModerator,
  ]);

  return (
    <ClientOnly>
      <RequireAuth>
        <>
          <Container className={style["profile-page"]}>
            <Row>
              <Col xs={12} offset={{ xl: 2 }} xl={8}>
                <div className={style["profile-head"]}>
                  <Avatar
                    size="md"
                    bg={"green.400"}
                    name={username}
                    src={avatar}
                    referrerPolicy="no-referrer"
                  />
                  <div style={{ flexDirection: "column", marginLeft: "15px" }}>
                    <div className={style.bread}>
                      <span>{username}</span>
                      <span className={style.separator}>/</span>
                      <span>{t("PROFILE")}</span>
                      <span className={style.separator}>/</span>
                      <span>{t("MENU.GENERAL")}</span>
                    </div>
                    {isAdmin && (
                      <span className={style.admin + " " + style["user-role"]}>
                        {t("ADMIN")}
                      </span>
                    )}
                    {isModerator && (
                      <span
                        className={style.moderator + " " + style["user-role"]}
                      >
                        {t("MODERATOR")}
                      </span>
                    )}
                    {!isAdmin && !isModerator && (
                      <span className={style.user}>{t("USER")}</span>
                    )}
                  </div>
                </div>
              </Col>
            </Row>
            <Row>
              <Col xs={12} offset={{ xl: 2 }} xl={8}>
                <div className={style["mobile-menu"]}>
                  <div className="select-container">
                    <select className="minimal">
                      <option>{t("MENU.GENERAL")}</option>
                      <option>{t("MENU.SOCIAL")}</option>
                      <option>{t("MENU.SECURITY")}</option>
                      <option>{t("MENU.NOTIFICATIONS")}</option>
                      <option>{t("MENU.MESSAGERY")}</option>
                    </select>
                  </div>
                </div>
                <div className={style.content}>
                  <div className={style["left-menu"]}>
                    <span
                      className={style.active}
                      onClick={() => router.push("/profile?general=true")}
                    >
                      {t("MENU.GENERAL")}
                    </span>
                    <span onClick={() => router.push("/profile?social=true")}>
                      {t("MENU.SOCIAL")}
                    </span>
                    <span onClick={() => router.push("/profile?security=true")}>
                      {t("MENU.SECURITY")}
                    </span>
                    <span
                      onClick={() => router.push("/profile?notifications=true")}
                    >
                      {t("MENU.NOTIFICATIONS")}
                    </span>
                    <span onClick={() => router.push("/profile?inbox=true")}>
                      {t("MENU.MESSAGERY")}
                    </span>
                  </div>
                  {router.query.general && router.query.general === "true" && (
                    <General />
                  )}
                  {router.query.social && router.query.social === "true" && (
                    <Social />
                  )}
                  {router.query.security &&
                    router.query.security === "true" && <Security />}
                  {router.query.notifications &&
                    router.query.notifications === "true" && <Notifications />}

                  {router.query.inbox && router.query.inbox === "true" && (
                    <Inbox />
                  )}
                </div>
              </Col>
            </Row>
          </Container>
          <div
            style={{
              display: "bottom",
            }}
          ></div>

          {router.query.general === "true" ||
          router.query.social === "true" ||
          router.query.security === "true" ||
          router.query.notifications === "true" ||
          router.query.inbox === "true" ? (
            <Footer />
          ) : (
            <></>
          )}
        </>
      </RequireAuth>
    </ClientOnly>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  const { locale } = context;
  return {
    props: {
      ...(await serverSideTranslations(locale ?? "fr", [
        "menu",
        "profile-page",
        "foot",
      ])),
    },
  };
};

export default ProfilePage;
