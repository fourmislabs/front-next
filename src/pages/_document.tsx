import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="en">
      <Head>
     
        <link href="/favicon.ico" rel="icon"/>
        <link rel="apple-touch-icon" href="/icons/icon-256x256.png" />
        <link rel="manifest" href="/manifest.json" />

      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
