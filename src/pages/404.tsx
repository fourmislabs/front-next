import styles from "../styles/pages/404-page.module.scss";
import Image from "next/image";
import illustration from "../../public/assets/img/illus/ant4.png";
import { useTranslation } from "next-i18next";
import router from "next/router";

function PageNotFound() {
  const { i18n } = useTranslation();

  return (
    <div className={styles["four-hundred-four"]}>
      <h1 className={"animate__animated animate__pulse animate__infinite"}>
        404
      </h1>
      <Image
        className={"illustration animate__animated animate__fadeIn"}
        src={illustration}
        alt="404 error"
        placeholder="blur"
        priority
      />
      {i18n.language === "fr" ? (
        <h5>Oh oh, on dirais que vous êtes perdu...</h5>
      ) : (
        <h5>Uh oh, looks like you&apos;re lost...</h5>
      )}

      <button
        className={"btn btn-green"}
        onClick={() => {
          router.push("/");
        }}
      >
        {i18n.language === "fr"
          ? "Revenir en lieu sûr"
          : "Go back to sa safer place"}
      </button>
    </div>
  );
}

export default PageNotFound;
