import type { GetStaticProps } from "next/types/index";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import ClientOnly from "@/components/ClientOnly";
import { RequireAdminRole } from "@/components/guard";
import { useState, useEffect } from "react";

const Admin = () => {
  const [data, setData] = useState<any>([]);

  useEffect(() => {
    fetch("/api/users/wallets", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(async (res) => {
        return res.text();
      })
      .then(function (data) {
        const i = JSON.parse(data);
        console.log(i);
        setData(i.res);
      });
  }, []);

  return (
    <>
      <ClientOnly>
        <RequireAdminRole>
          <>
            <h2>Wallets list</h2>
            <div>
              {data.map((element: any, i: number) => {
                return <p key={element._id}>ProfileId : {element.profileId}</p>;
              })}
            </div>
          </>
        </RequireAdminRole>
      </ClientOnly>
    </>
  );
};

export default Admin;

export const getStaticProps: GetStaticProps = async (context) => {
  return {
    props: {
      ...(await serverSideTranslations(context.locale ?? "fr", [
        "admin-page",
        "menu",
        "foot",
      ])),
    },
  };
};
