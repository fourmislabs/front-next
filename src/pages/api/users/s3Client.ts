import { S3Client } from "@aws-sdk/client-s3"; // ES Modules import

const config = {
  region: process.env.NEXT_PUBLIC_S3_UPLOAD_REGION || "eu-central-1",
  credentials: {
    accessKeyId: process.env.NEXT_PUBLIC_S3_UPLOAD_KEY || "",
    secretAccessKey: process.env.NEXT_PUBLIC_S3_UPLOAD_SECRET || "",
  },
};

const client = new S3Client(config);
export default client
