import type { NextApiRequest, NextApiResponse } from "next";
import { AuthFetch } from "../lib/fetcher";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  const response = await AuthFetch("GET", "wallets", req, res);
  const data = await Promise.resolve(response);
  if (data === "session expired") {
    return res.status(401).json({ message: "session expired" });
  } else {
    return res.status(200).json(data);
  }
}
