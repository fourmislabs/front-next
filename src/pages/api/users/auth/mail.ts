import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(req: NextApiRequest, res: NextApiResponse)  {
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_BACKEND_BASE_URL}/mail/activate?key=${req.body.key}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  const data = await response.json();
  console.log("data", data);
  if (data && data.exp < Date.now() / 1000) {
    return res.status(403).send("Activation link expired");
  } else if (data && data.exp > Date.now() / 1000) {
    return res.status(200).send("Account activated");
  } else if (!data) {
    return res.status(403).send("Activation link expired");
  }
};
