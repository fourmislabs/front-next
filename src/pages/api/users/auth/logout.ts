import { deleteCookie } from "cookies-next";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  NextApiRequest: NextApiRequest,
  NextApiResponse: NextApiResponse<any>
) {
  try {
    let response = await fetch(
      `${process.env.NEXT_PUBLIC_BACKEND_BASE_URL}/auth/logout/${NextApiResponse.req.headers["user-id"]}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (response.status === 200) {
      deleteCookie("user", { req: NextApiRequest, res: NextApiResponse });
      deleteCookie("accessToken", {
        req: NextApiRequest,
        res: NextApiResponse,
      });
      deleteCookie("refreshToken", {
        req: NextApiRequest,
        res: NextApiResponse,
      });

      return NextApiResponse.status(200).send("Logged Out");
    } else if (response.status === 401) {
      deleteCookie("user", { req: NextApiRequest, res: NextApiResponse });
      deleteCookie("accessToken", {
        req: NextApiRequest,
        res: NextApiResponse,
      });
      deleteCookie("refreshToken", {
        req: NextApiRequest,
        res: NextApiResponse,
      });
      return NextApiResponse.status(200).send("logged out");
    }
  } catch (err) {
    console.log(err);
    return NextApiResponse.status(500).send("Internal Server Error");
  }
}
