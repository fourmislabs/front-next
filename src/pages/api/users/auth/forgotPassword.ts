import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  if (req.method === "POST") {
    let response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_BASE_URL}/auth/forgotPassword`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email: req.body }),
    });
    if (response.ok) {
      res.status(200).json({ message: "Email Sent" });
    } else {
      console.log("response", await response.json());
      res.status(400).json({ message: "Email not sent" });
    }
  } else if (req.method === "PATCH") {
    let response = await fetch(
      `${process.env.NEXT_PUBLIC_BACKEND_BASE_URL}/auth/updatepassword?key=${req.query.key}` ,
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ password: req.body }),
      }
    );
    if (response.ok) {
      res.status(200).json({ message: "Password Reset" });
    } else {
      res.status(400).json({ message: "Password not reset" });
    }
  }
}
