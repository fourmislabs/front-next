import { getCookie, setCookie } from "cookies-next";
import { NextApiRequest, NextApiResponse } from "next";
import { fetchAuth } from "../../lib/fetcher";

export default async function handler(
  NextApiRequest: NextApiRequest,
  NextApiResponse: NextApiResponse<any>
) {
  const res = await fetchAuth("POST", NextApiRequest.body, "login");
  const data = await res.json();
  if (data.tokens) { 
    setCookie("user", data.user, {
      req: NextApiRequest,
      res: NextApiResponse,
    });
    setCookie("accessToken", data.tokens.accessToken, {
      req: NextApiRequest,
      res: NextApiResponse,
    });
    setCookie("refreshToken", data.tokens.refreshToken, {
      req: NextApiRequest,
      res: NextApiResponse,
    });
    return NextApiResponse.status(200).json(data);
  } else
    return NextApiResponse.status(401).json({ message: "invalid credentiels" });
}
