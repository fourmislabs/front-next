import { setCookie } from "cookies-next";
import { NextApiRequest, NextApiResponse } from "next";
import { fetchAuth } from "../../lib/fetcher";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  const response = await fetchAuth("POST", req.body, "google");
  const data = await response.json();
  if (data.tokens) {
    setCookie("accessToken", data.tokens.accessToken, {
      req,
      res,
    });
    setCookie("refreshToken", data.tokens.refreshToken, {
      req,
      res,
    });
    setCookie("user", data.user, {
      req,
      res,
    });
    return res.status(200).json(data);
  } else return res.status(401).json({ message: "invalid credentiels" });
}
