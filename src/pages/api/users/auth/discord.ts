import { setCookie } from "cookies-next";
import { NextApiRequest, NextApiResponse } from "next";
import { fetchAuth } from "../../lib/fetcher";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  let string = "";
  const data = {
    client_id: process.env.NEXT_PUBLIC_DISCORD_CLIENT_ID,
    client_secret: process.env.NEXT_PUBLIC_DISCORD_SECRET,
    grant_type: "authorization_code",
    code: req.body.code,
    redirect_uri: process.env.NEXT_PUBLIC_BASE_URL + "/auth?login=true",
  };
  for (const [key, value] of Object.entries(data)) {
    if (!value) continue;
    string += `&${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
  }
  const requestToken = await fetch(`https://discord.com/api/oauth2/token`, {
    method: "POST",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: string.substring(1),
  });
  const requestTokenObj = await requestToken.json();
  const verifyToken = await fetch("https://discord.com/api/users/@me", {
    headers: {
      authorization: `Bearer ${requestTokenObj.access_token}`,
    },
  });
  const verifyTokenObj = await verifyToken.json();
  const response = await fetchAuth("POST", verifyTokenObj, "discord");
  const datax = await response.json();
  if (datax.tokens) {
    setCookie("user", datax.user, {
      req,
      res,
    });
    setCookie("accessToken", datax.tokens.accessToken, {
      req,
      res,
    });
    setCookie("refreshToken", datax.tokens.refreshToken, {
      req,
      res,
    });
    return res.status(200).json(datax);
  } else {
    return res.status(401).json(datax);
  }
}
