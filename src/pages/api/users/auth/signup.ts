import { NextApiRequest, NextApiResponse } from "next";
import { fetchAuth } from "../../lib/fetcher";

export default async function handler(
  NextApiRequest: NextApiRequest,
  NextApiResponse: NextApiResponse<any>
) {
  const res = await fetchAuth("POST", NextApiRequest.body, "signup");
  const data = await res.json();
  if (data._id) return NextApiResponse.status(201).json({ message: "success" });
  else if (res.status === 409)
  return NextApiResponse.status(409).json({ message: "user already exist" });
  else if (res.status === 400)
    return NextApiResponse.status(400).json({ message: "invalid credentiels" });
  else return NextApiResponse.status(500).json({ message: "server error" });
}
