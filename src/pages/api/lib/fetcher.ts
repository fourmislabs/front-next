import { deleteCookie, getCookie, setCookie } from "cookies-next";
import { NextApiRequest, NextApiResponse } from "next";


const accessToken = (req: NextApiRequest, res: NextApiResponse) => {
  return getCookie("accessToken", {
    req,
    res,
  });
};

const refreshToken = (req: NextApiRequest, res: NextApiResponse) => {
  return getCookie("refreshToken", {
    req,
    res,
  });
};
const fetchAuth = async (
  method: string,
  body: string,
  endPoint: string
): Promise<any> => {
  return await fetch(
    `${process.env.NEXT_PUBLIC_BACKEND_BASE_URL}/auth/${endPoint}`,
    {
      method: method,
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    }
  );
};


const user = (req: NextApiRequest, res: NextApiResponse) => {
  return getCookie("user", {
    req,
    res,
  });
};

const AuthFetch = async (
  method: string,
  endPoint: string,
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const Token = accessToken(req, res);
  let serverResponse: any;
  let aux 
  
  if (method !=="GET"){
  aux = await fetch(
    `${process.env.NEXT_PUBLIC_BACKEND_BASE_URL}/${endPoint}`,
    {
      method: method,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${Token}`,
      },
      body: JSON.stringify(req.body),
    }
  );
  }
  else if (method === "GET") {
    aux = await fetch(
      `${process.env.NEXT_PUBLIC_BACKEND_BASE_URL}/${endPoint}`,
      {
        method: method,
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${Token}`,
        },
      }
    );

  }
  const data = await aux?.json();
  if (aux?.status === 200) {
    return data;
  } else if (data.statusCode === 401) {
    const Token = refreshToken(req, res);
    await fetch(
      `${process.env.NEXT_PUBLIC_BACKEND_BASE_URL}/auth/refreshtoken`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${Token}`,
        },
      }
    )
      .then(async (x) => {
        serverResponse = await x.json();
        if (serverResponse.newToken) {
          deleteCookie("accessToken", { req, res });
          setCookie("accessToken", serverResponse.newToken, { req, res });
           if (method !=="GET"){
          const x = await fetch(
            `${process.env.NEXT_PUBLIC_BACKEND_BASE_URL}/${endPoint}`,
            {
              method: method,
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${serverResponse.newToken}`,
              },
              body: JSON.stringify(req.body) ,
            }
          )
          serverResponse = await x.json()
           }else 

           {
            const x = await fetch(
              `${process.env.NEXT_PUBLIC_BACKEND_BASE_URL}/${endPoint}`,
              {
                method: method,
                headers: {
                  "Content-Type": "application/json",
                  Authorization: `Bearer ${serverResponse.newToken}`,
                },
              }
            )
            serverResponse = await x.json()
             }
        } else if (!serverResponse.newToken) {
          console.log(serverResponse)
          console.log(Token)
          deleteCookie("accessToken", { req, res });
          deleteCookie("refreshToken", { req, res });
          deleteCookie("user", { req, res });
          serverResponse = "session expired";
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  return serverResponse;
};

const clientFetch = async (
  method: string,
  endPoint: string,
  body?: any,
  callback?: Function
) => {
  fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/${endPoint}`, {
    method,
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body) ?? undefined,
  }).then(async (x) => {
    if (x.status === 401) {
      deleteCookie("accessToken");
      deleteCookie("refreshToken");
      deleteCookie("user");
      window.location.href = `${process.env.NEXT_PUBLIC_BASE_URL}/auth?login=true`;
      return x;
    }
    return x;
  });
};

export { fetchAuth, user, accessToken, refreshToken, AuthFetch, clientFetch };
