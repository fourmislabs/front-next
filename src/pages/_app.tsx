import { Container, setConfiguration } from "react-grid-system";
import "../styles/global.scss";
import { hasCookie } from "cookies-next";
import Head from "next/head";
import dynamic from "next/dynamic";
import { ChakraProvider } from "@chakra-ui/react";
import { appWithTranslation } from "next-i18next";
import { Exo } from "next/font/google";
import { AppProps } from "next/app";
import { extendTheme } from "@chakra-ui/react";
import { useRouter } from "next/router";

const Banner = dynamic(() => import("../components/header/banner/Banner"), {});

const Menu = dynamic(() => import("../components/header/menu/Menu"));

const exo = Exo({
  subsets: ["latin"],
  weight: ["400", "500", "700"],
  // variable: "--archivo-font",
});

const theme = extendTheme({
  colors: {
    red: {
      400: "#ef0909",
      600: "8d0909",
      900: "#610808",
    },
    blue: {
      400: "#357ac9",
    },
    green: {
      100: "#3ce715",
      400: "#45be29",
      600: "#3d9629",
      900: "#327b21",
    },
  },
});

const App = ({ Component, pageProps: pageProps }: AppProps) => {
  const router = useRouter();

  setConfiguration({ breakpoints: [767, 992, 1200, 1600] });

  return (
    <ChakraProvider theme={theme} resetCSS={true}>
      <>
        <Head>
          <title>Fourmislabs</title>
          <link rel="preconnect"></link>
          <link rel="apple-touch-icon" href="/icons/icon-256x256.png" />
          <meta charSet="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta
            name="description"
            content="Fourmislabs, système de gestion de fondations et colonies"
          />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <meta name="keywords" content="ant colony management system" />
          <meta name="theme-color" content="#45be29" />
          <meta name="msapplication-navbutton-color" content="#45be29" />
        </Head>
      </>
      <Container
        className={exo.className}
        fluid
        style={{ position: "inherit", padding: 0 }}
      >
        {router.pathname !== "/404" && (
          <>
            {hasCookie("user") ? (
              <Menu tr={pageProps} />
            ) : (
              <Banner tr={pageProps} />
            )}
          </>
        )}
        <Component {...pageProps} />
      </Container>
    </ChakraProvider>
  );
};

export default appWithTranslation(App);
