import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { GetStaticProps } from "next";
import dynamic from "next/dynamic";
import { hasCookie } from "cookies-next";

const LandingPage = dynamic(
  () => import("@/components/landig-page/LandingPage"),
  {
    loading: () => <></>,
  }
);

const Home = dynamic(() => import("@/components/home/Home"), {
  loading: () => <></>,
});

const Index = ({ tr }: { tr: any }) => {
 const user = hasCookie("user");

  return (
    <>
      {hasCookie("user") ? (
        <>
          <Home tr={tr} />{" "}
        </>
      ) : (
        <>
          <LandingPage tr={tr} />{" "}
        </>
      )}
    </>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  const { locale } = context;

  return {
    props: {
      ...(await serverSideTranslations(locale ?? "fr", [
        "banner",
        "landing-page",
        "menu",
        "foot",
      ])),
    },
  };
};

export default Index;
