import { GetStaticProps } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import {useRouter} from "next/router";
import dynamic from "next/dynamic";
import ClientOnly from "@/components/ClientOnly";
import { PublicRoute } from "@/components/guard";


const Login = dynamic(() => import("../components/auth/login/Login"), {
  loading: () => <></>,
});

const Signup = dynamic(() => import("../components/auth/signup/Signup"), {
  loading: () => <></>,
});
const Backup = dynamic(() => import("../components/auth/backup/Backup"), {
  loading: () => <></>,
});


const Index = (props: any) => {
  const router = useRouter();


  return (
    <>
      <ClientOnly>
        <PublicRoute>
          <>
            {(router.query.login && router.query.login === "true") && <Login tr={props} /> }
            {(router.query.signup && router.query.signup === "true") && <Signup tr={props} />}
            {(router.query.backup && router.query.backup === "true") && <Backup tr={props} />}
          </>
        </PublicRoute>
      </ClientOnly>
    </>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  const { locale } = context;
  return {
    props: {
      ...(await serverSideTranslations(locale ?? "fr", [
        "foot",
        "banner",
        "auth-page",
        "reset-password",
      ])),
    },
  };
};

export default Index;
