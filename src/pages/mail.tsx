import { useCallback, useEffect, useState } from "react";
import { useUpdateEffect } from "@chakra-ui/react";
import { GetStaticProps } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import ClientOnly from "@/components/ClientOnly";

export default function Mail(props: any) {
  const { t } = useTranslation("activation");

  const [key, setKey] = useState("");
  const [data, setData] = useState("");
  const [activated, setActivated] = useState(false);

  const verifyUser = useCallback(
    async (key: string) => {
      try {
        const response = await fetch("/api/users/auth/mail", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ key }),
        });
        if (!response.ok) {
          setActivated(false);
          setData(t("ERROR_ACTIVATED")!);
          return;
        } else if (response.ok) {
          setActivated(true);
          setData(t("SUCCESS")!);
          return;
        }
      } catch (err: any) {
        setActivated(false);
        setData(t("ERROR_ACTIVATED")!);
      }
    },
    [t]
  );
  useEffect(() => {
    const key = window.location.href.split("=")[1];
    setKey(key!);
  }, []);
  useUpdateEffect(() => {
    verifyUser(key);
  }, [key]);

  return (
    <>
      <ClientOnly>
        {activated && (
          <div>
            {/* <i className={}></i> */}
            <span>{data}</span>
          </div>
        )}
        <span>{data}</span>
      </ClientOnly>
    </>
  );
}

export const getStaticProps: GetStaticProps = async (context) => {
  const { locale } = context;

  return {
    props: {
      ...(await serverSideTranslations(locale ?? "fr", [
        "banner",
        "menu",
        "activation",
        "foot",
        "foot"
      ])),
    },
  };
};

