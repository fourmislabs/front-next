import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { GetStaticProps } from "next/types";
import Footer from "@/components/footer/Footer";

const nests = () => {
  return (
    <>
    <div>nests</div>
    <div className="f"><Footer/>
    <style jsx>{`
        .f {
          margin-top: 100%;
        }
      `}</style>
    </div>
    </>
  )
}
export const getStaticProps: GetStaticProps = async (context) => {
    return {
      props: {
        ...(await serverSideTranslations(context.locale ?? "fr", [
          "menu",
          "foot",
        ])),
      },
    };
  };
  

export default nests