enum EI18n {
  FRENCH = 'fr',
  ENGLISH = 'en',
}

export const languages = Object.values(EI18n);

export const DEFAULT_LANGUAGE = EI18n.FRENCH;
