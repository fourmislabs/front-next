const USERNAME_MIN_LENGTH = 4;
const USERNAME_MAX_LENGTH = 24;
const PASSWORD_MIN_LENGTH = 8;
const PASSWORD_MAX_LENGTH = 30;
const PASSWORD_REGEX = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
const EMAIL_REGEX: RegExp =
  /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const USERNAME_REGEX: RegExp = /^[a-zA-Z0-9_-]*$/;

export const validateUsername = (username: string): boolean => {
  if (
    username.length < USERNAME_MIN_LENGTH ||
    username.length > USERNAME_MAX_LENGTH ||
    !username.match(USERNAME_REGEX)
  ) {
    return false;
  }
  return true;
};

export const validatePasword = (password: string): boolean => {
  // TODO add more password validation
  if (password.match(PASSWORD_REGEX)) {
    return false;
  } else if (
    password.length < PASSWORD_MIN_LENGTH ||
    password.length > PASSWORD_MAX_LENGTH
  ) {
    return false;
  }
  return true;
};

export const validateEmail = (email: string): boolean => {
  if (email.match(EMAIL_REGEX)) {
    return true;
  }
  return false;
};
