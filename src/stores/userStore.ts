import { hasCookie } from "cookies-next";
import { create } from "zustand";
import { persist } from "zustand/middleware";

interface userState {
  userId: string;
  username: string;
  email: string;
  avatar?: string;
  isAdmin: boolean;
  isModerator: boolean;
  isConnected: boolean;
  profileId: string;
  setUserId: (userId: string) => void;
  setUsername: (username: string) => void;
  setEmail: (email: string) => void;
  setAvatar: (avatar: string) => void;
  setIsAdmin: (bool: boolean) => void;
  SetIsModerator: (bool: boolean) => void;
  setIsConnected: (bool: boolean) => void;
  setProfileId: (id: string) => void;
  initStore: () => void;
}

const useUserStore = create<userState>()(
  persist(
    (set) => ({
      userId: "",
      username: "",
      email: "",
      avatar: "",
      isAdmin: false,
      isModerator: false,
      isConnected: hasCookie("user"),
      profileId: "",
      setUserId: (ch: string) => set((state) => ({ userId: ch })),
      setUsername: (ch: string) => set((state) => ({ username: ch })),
      setEmail: (ch: string) => set((state) => ({ email: ch })),
      setAvatar: (url: string) => set((state) => ({ avatar: url })),
      setIsAdmin: (bool: boolean) => set((state) => ({ isAdmin: bool })),
      SetIsModerator: (bool: boolean) =>
        set((state) => ({ isModerator: bool })),
      setIsConnected: (bool: boolean) =>
        set((state) => ({ isConnected: bool })),
      setProfileId: (id: string) => set((state) => ({ profileId: id })),
      initStore: () =>
        set((state) => ({
          userId: "",
          username: "",
          email: "",
          isAdmin: false,
          isModerator: false,
          isConnected: false,
          profileId: "",
        })),
    }),
    {
      name: "userStore",
    }
  )
);

export { useUserStore };
