import { getCookie, hasCookie } from "cookies-next";
import router from "next/router";
import { ERoleName } from "../utils/enums/roles.enum";

export const RequireAuth = ({ children }: { children: JSX.Element }) => {
  if (!hasCookie("user")) {
    router.push("/404");
    return <></>;
  } else return <>{children}</>;
};

export const PublicRoute = ({ children }: { children: JSX.Element }) => {
  const user = getCookie("user");
  if (user) {
    router.push("/");
    return <></>;
  } else return <>{children}</>;
};

export const RequireAdminRole = ({ children }: { children: JSX.Element }) => {
  let userRole;
  try {
    userRole = JSON.parse(getCookie("user") as string);
  } catch (e) {
    router.push("/404");
    return <></>;
  }
  if (!userRole?.roles?.includes(ERoleName.ADMIN)) {
    router.push("/404");
    return <></>;
  } else return <>{children}</>;
};

export const RequireModeratorRole = ({
  children,
}: {
  children: JSX.Element;
}) => {
  let userRole;
  try {
    userRole = JSON.parse(getCookie("user") as string);
  } catch (e) {
    router.push("/404");
    return <></>;
  }
  if (!userRole?.roles?.includes(ERoleName.MODERATOR)) {
    router.push("/404");
    return <></>;
  } else return <>{children}</>;
};
