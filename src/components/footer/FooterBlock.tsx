import style from "./footer-block.module.scss";

type ItemData = {
  section: string;
  links: string[];
};

const FooterBlock = (props: ItemData) => {
  return (
    <div className={style.section}>
      <span>
        {props.section}
      </span>
      <div className={style["link-container"]}>
        {props.links.map((link) => {
          return (
            <span key={link} className={style.link}>
              {link}
            </span>
          );
        })}
      </div>
    </div>
  );
};

export default FooterBlock;
