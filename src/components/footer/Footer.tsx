import style from "./footer.module.scss";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { Col, Row } from "react-grid-system";
import logoFooter from "../../../public/assets/img/footer/logo-footer.png";
import patreon from "../../../public/assets/img/footer/patreon.svg";
import instagram from "../../../public/assets/img/footer/instagram.svg";
import facebook from "../../../public/assets/img/footer/facebook.svg";
import discord from "../../../public/assets/img/footer/discord.svg";
import FooterBlock from "./FooterBlock";

const Footer = () => {
  const { t } = useTranslation("foot");

  const socials = [
    {
      name: "Patreon",
      url: patreon,
      bg: "#e7725b",
      fg: "#c54e37",
    },
    {
      name: "Instagram",
      url: instagram,
      bg: "#e73ddd",
      fg: "#bc30b4",
    },
    {
      name: "Facebook",
      url: facebook,
      bg: "#4883fb",
      fg: "#3667cc",
    },
    {
      name: "Discord",
      url: discord,
      bg: "#627cbe",
      fg: "#4f6497",
    },
  ];
  const links = [
    {
      section: t("RESELERS"),
      links: [t("CREATE_SHOP"), t("ADVANTAGES")],
    },
    {
      section: t("CONTRIBUTE"),
      links: [
        t("CORRECT_VALIDATE"),
        t("WRITE_ARTICLE"),
        t("BECOME_MODERATOR"),
        t("WEBDEVELOPERS"),
        t("BUY_US_A_COFFEE"),
      ],
    },
    {
      section: t("PARTNERSHIP"),
      links: [t("OUR_PARTNERS"), t("BECOME_PARTNER")],
    },
    {
      section: t("ABOUT"),
      links: [t("FOURMISLABS_TEAM"), t("CONTACT"), t("LEGAL"), t("CGU")],
    },
  ];
  return (
    <Row style={{ margin: 0, alignItems: "center" }} className={style.footer}>
      <Col xs={12} sm={4} md={4} lg={3}>
        <div className={style["brand-container"]}>
          <div className={style["logo-container"]}>
            <Image
              width={224}
              height={224}
              style={{ zIndex: 1 }}
              className={"animate__animated animate__fadeIn"}
              src={logoFooter}
              alt="logo-footer"
              placeholder="blur"
            />
            <span className={style["brand-name"]}>
              FOURMIS<span>LABS</span>
            </span>
          </div>
          <div className={style.social}>
            {socials.map((social) => {
              const socialName = social.name;
              return (
                <div
                  style={{
                    zIndex: 1,
                    backgroundColor: social.bg,
                    borderBottom: `3px solid ${social.fg}`,
                  }}
                  key={socialName}
                  className={
                    style["background-social"] + " " + "animate__animated"
                  }
                >
                  <Image
                    width={30}
                    height={30}
                    style={{ zIndex: 1 }}
                    className={
                      style.icon +
                      " " +
                      "animate__animated animate__fadeIn animate__delay-03s"
                    }
                    src={social.url}
                    alt={socialName}
                    priority
                  />
                </div>
              );
            })}
          </div>
        </div>
      </Col>
      <Col
        style={{
          display: "flex",
          justifyContent: "space-around",
          padding: "40px 0",
        }}
        xs={12}
        sm={8}
        md={8}
        lg={9}
      >
        {links.map((link) => {
          return (
            <FooterBlock
              key={link.section}
              section={link.section}
              links={link.links}
            />
          );
        })}
      </Col>
      <div className={style.separator} />
      <span>© 2023 {t("ALL_RIGHTS_RESERVED")}</span>
    </Row>
  );
};

export default Footer;
