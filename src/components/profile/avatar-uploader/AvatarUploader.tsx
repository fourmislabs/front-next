import { useUserStore } from "@/stores/userStore";
import { PutObjectCommand, DeleteObjectCommand } from "@aws-sdk/client-s3";
import client from "../../../pages/api/lib/s3Client";
import { clientFetch } from "../../../pages/api/lib/fetcher";

import { useCallback, useState } from "react";
const AvatarUploader = () => {
  const [file, setFile] = useState<File | null>(null);
  const [avatar, userId, setAvatar, setIsConnected, profileId] = useUserStore(
    (state) => [
      state.avatar,
      state.userId,
      state.setAvatar,
      state.setIsConnected,
      state.profileId,
    ]
  );
  const [oldAv, setOldAv] = useState("");

  const onFileChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      event.target.files && setFile(event.target.files[0]);
    },
    [setFile]
  );

  const uploadFile = useCallback(async () => {
    if (!file) return;
    const params = {
      Bucket: "fmlb",
      Key: userId + "/avatar/" + file.name.split(" ").join(""),
      Body: file,
    };
    const command = new PutObjectCommand(params);
    await client
      .send(command)
      .then(async (data) => {
        setOldAv(avatar!.split("com/")[1])
        setAvatar(
          `${process.env.NEXT_PUBLIC_S3_BASE_URL}/${userId}/avatar/${file.name
            .split(" ")
            .join("")}`
        );
        await clientFetch("PATCH", "/users/updatefield", {
          profileId,
          avatar: `${
            process.env.NEXT_PUBLIC_S3_BASE_URL
          }/${userId}/avatar/${file.name.split(" ").join("")}`,
        });
      })
      .then(async () => {
          const params = {
            Bucket: "fmlb",
            Key: oldAv,
          };
          const command = new DeleteObjectCommand(params);
          await client.send(command).then((res) => {
            console.log(res);
          });
        }
      )
      .catch((err) => {
        console.log(err);
      });
  }, [file, setAvatar, userId, profileId,avatar,oldAv]);
  return (
    <div>
      <input type="file" onChange={onFileChange} />
      <button onClick={uploadFile} onBeforeInput={() => {}}>
        Upload
      </button>
    </div>
  );
};

export default AvatarUploader;
