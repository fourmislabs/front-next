import { useTranslation } from "next-i18next"
import AvatarUploader from "../avatar-uploader/AvatarUploader"

const General = () => {
    const { t } = useTranslation(["profile-page"])

  return (
    <div className="formulaire">
    <div className="form-avatar">
      {/* <div
  style={
    avatar === 'https://noavatarurl.fmb' || avatar === ''
      ? {
          backgroundImage: `url(${
            require('../../assets/img/header/avatar.svg')
              .default
          }`,
        }
      : { backgroundImage: `url(${avatar})` }
  }
  className="avatar"
/> */}
      <AvatarUploader
      // handleDeleteClick={handleDeleteClick}
      // dir="avatar"
      // isResourceValidated={() => true}
      />
    </div>
    <form action="">
      <div className="form-block">
        <label>{t("INPUTS_LABELS.PSEUDO")}</label>
        <input type="text" placeholder="" />
      </div>
      <div className="form-block">
        <div className="multi-inputs">
          <div className="select-container">
            <label>{t("INPUTS_LABELS.YEARS_EXPERIENCE")}</label>
            {/* //TODO replace by enum */}
            <select className="minimal">
              <option>0</option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10+</option>
            </select>
          </div>
          <div className="select-container">
            <label>{t("INPUTS_LABELS.FAVORITE_SPECIES")}</label>
            {/* <input
        placeholder={t('TYPE_SPECIES_NAME').toString()}
        type="text"
        onChange={handleSearch}
        onBlur={() => onSpeciesBlur()}
        onFocus={() => onSpeciesFocus()}
        ref={searchRef}
      />
      {isSearch && data.length > 0 && (
        <div className="search-species-results animate__animated animate__fadeIn">
          {data.map((element, i) => {
            return (
              <p key={i}>
                {JSON.parse(JSON.stringify(element)).name}
              </p>
            );
          })}
        </div>
      )} */}
          </div>
        </div>
      </div>
      <div className="form-block">
        <label>{t("INPUTS_LABELS.SEX")}</label>
        <div className="select-container">
          <select className="minimal">
            <option>{t("MALE")}</option>
            <option>{t("FEMALE")}</option>
            <option>{t("OTHER")}</option>
          </select>
        </div>
      </div>
      <div className="form-block">
        <label>{t("INPUTS_LABELS.DATE_OF_BIRTH")}</label>
        <input type="date" placeholder="" />
      </div>
      <div className="form-block">
        <div className="multi-inputs">
          <div className="select-container">
            <label>{t("ADRESS.COUNTRY")}</label>
            <select className="minimal"></select>
          </div>
          <div className="select-container">
            <label>{t("ADRESS.CITY")}</label>
            <select className="minimal"></select>
          </div>
          <div className="select-container">
            <label>{t("ADRESS.POSTAL_CODE")}</label>
            <input type="text" placeholder="" />
          </div>
        </div>
      </div>
      <div className="form-block">
        <label>{t("ADRESS.NUMBER_STREET")}</label>
        <input type="text" placeholder="" />
        <span className="disclaimer">
          {t("ADRESS.DISCLAIMER")}
        </span>
        {/* <Link href="/" className="sub-disclaimer">
    {t('CONSULT_DATA_PROTECTION')}
  </Link> */}
      </div>
      <div className="form-block">
        <label> {t("INPUTS_LABELS.WEBSITE")}</label>
        <div className="web-input">
          <div className="url">
            <span>https://</span>
          </div>
          <input
            className="website"
            type="text"
            placeholder=""
          />
        </div>
      </div>
      <div
        style={{ marginBottom: "1.5rem" }}
        className="form-block"
      >
        <label>{t("INPUTS_LABELS.BIO")}</label>
        <br />
        <span className="count-words">0 / 1024</span>
        <textarea rows={7} cols={5} name="" id=""></textarea>
      </div>
      <div className="foot">
        <button className="btn btn-green">{t("SAVE")}</button>
      </div>
    </form>
  </div>
  )
}

export default General