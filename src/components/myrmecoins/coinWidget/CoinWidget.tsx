import style from "./coin-widget.module.scss";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { Col, Row } from "react-grid-system";
import coin from "../../../../public/assets/img/coin.png";

const CoinWidget = (tr:any) => {
  const { t } = useTranslation("myrmecoins");

  return (
    <div className={style['coin-widget']}>
      <div>
      <Image
        width={25}
        height={25}
        style={{ zIndex: 1}}
        className={
          style.icon
        }
        src={coin}
        alt={'myrmecoin-icon'}
        priority
        />
        <span>845</span>
    </div>
    </div>
  );
};

export default CoinWidget;
