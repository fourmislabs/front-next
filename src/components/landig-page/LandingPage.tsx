import style from "../../styles/pages/landing-page.module.scss";
import antOne from "../../../public/assets/img/illus/ant1.png";
import antTwo from "../../../public/assets/img/illus/ant2.png";
import antThree from "../../../public/assets/img/illus/ant3.png";
import bgOne from "../../../public/assets/img/illus/1.png";
import bgTwo from "../../../public/assets/img/illus/2.png";
import bgThree from "../../../public/assets/img/illus/3.png";
import foundations from "../../../public/assets/img/landing-page/icons-bandeau/ant-foundation.svg";
import colonies from "../../../public/assets/img/landing-page/icons-bandeau/ant-colony.svg";
import users from "../../../public/assets/img/landing-page/icons-bandeau/user.svg";
import breedingSheets from "../../../public/assets/img/landing-page/icons-bandeau/breedsheet.svg";
import foundationsOn from "../../../public/assets/img/landing-page/icons-bandeau/ant-foundation-color.svg";
import coloniesOn from "../../../public/assets/img/landing-page/icons-bandeau/ant-colony-color.svg";
import usersOn from "../../../public/assets/img/landing-page/icons-bandeau/user-color.svg";
import breedingSheetsOn from "../../../public/assets/img/landing-page/icons-bandeau/breedsheet-color.svg";
import { Container } from "react-grid-system";
import { useTranslation } from "next-i18next";
import { useMediaQuery } from "@chakra-ui/react";
import { useCallback, useEffect, useState } from "react";
import ClientOnly from "../ClientOnly";
import Slide from "./landing-slide/Slide";
import Bandeau from "./landing-bandeau/bandeau";
import dynamic from "next/dist/shared/lib/dynamic";

const Footer = dynamic(() => import("../footer/Footer"), {
  loading: () => <></>,
});

const LandingPage = ({ tr }: { tr: any }) => {
  const { t } = useTranslation(["landing-page"]);

  const [mobileView] = useMediaQuery("(max-width: 767px)", { ssr: true });
  const [isBlock2Hidden, setIsBlock2Hidden] = useState(true);
  const [isBlock3Hidden, setIsBlock3Hidden] = useState(true);
  const [isBlock4Hidden, setIsBlock4Hidden] = useState(true);

  const heightToShowBlock2Mobile = 340;
  const heightToShowBlock3Mobile = 1040;
  const heightToShowBlock4Mobile = 1000;

  const heightToShowBlock2 = 200;
  const heightToShowBlock3 = 700;
  const heightToShowBlock4 = 800;

  const listenToScroll = useCallback(() => {
    const winScroll =
      document.body.scrollTop || document.documentElement.scrollTop;
    if (!mobileView) {
      if (winScroll > heightToShowBlock2) {
        setIsBlock2Hidden(false);
      }
      if (winScroll > heightToShowBlock3) {
        setIsBlock3Hidden(false);
      }
      if (winScroll > heightToShowBlock4) {
        setIsBlock4Hidden(false);
      }
    } else {
      if (winScroll > heightToShowBlock2Mobile) {
        setIsBlock2Hidden(false);
      }
      if (winScroll > heightToShowBlock3Mobile) {
        setIsBlock3Hidden(false);
      }
      if (winScroll > heightToShowBlock4Mobile) {
        setIsBlock4Hidden(false);
      }
    }
  }, [
    mobileView,
    heightToShowBlock2,
    heightToShowBlock3,
    heightToShowBlock4,
    heightToShowBlock2Mobile,
    heightToShowBlock3Mobile,
    heightToShowBlock4Mobile,
  ]);

  useEffect(() => {
    window.addEventListener("scroll", listenToScroll);
    return () => window.removeEventListener("scroll", listenToScroll);
  }, [listenToScroll]);

  const slides = [
    {
      section: "white__section",
      title: [t("TITLE_1"), t("BUT"), t("WORD_1")],
      speech: [t("SPEECH_1-1"), t("SPEECH_1-2")],
      illu: antTwo,
      background: bgOne,
    },
    {
      section: "grey__section",
      title: [t("MORE_THAN"), "15840", t("TITLE_2")],
      speech: [t("SPEECH_2"), t("SPEECH_2-1")],
      illu: antOne,
      background: bgTwo,
      hidden: isBlock2Hidden,
    },
    {
      section: "white__section",
      title: [t("TITLE_3_1"), t("TITLE_3_2"), t("SEVERAL")],
      speech: [t("SPEECH_3-1"), t("SPEECH_3-2")],
      illu: antThree,
      background: bgThree,
      hidden: isBlock3Hidden,
    },
  ];

  const bandeauItems = [
    {
      title: t("FOUNDATIONS"),
      illu: [foundations, foundationsOn],
      stat: 723,
    },
    {
      title: t("COLONIES"),
      illu: [colonies, coloniesOn],
      stat: 1254,
    },
    {
      title: t("USERS"),
      illu: [users, usersOn],
      stat: 7856,
    },
    {
      title: t("BREEDING_SHEETS"),
      illu: [breedingSheets, breedingSheetsOn],
      stat: 345,
    },
  ];

  return (
    <ClientOnly>
      <>
        {/* <Banner tr={tr} /> */}
        <>
          <Container fluid className={style["landing-page"]}>
            {slides.map((slide, index) => {
              const slideItem = (
                <Slide
                  id={"slide-" + index}
                  key={"slide-" + index}
                  section={slide.section}
                  title={slide.title}
                  speech={slide.speech}
                  illu={slide.illu}
                  background={slide.background}
                  hidden={slide.hidden}
                />
              );
              return slideItem;
            })}
            {!isBlock4Hidden && <Bandeau items={bandeauItems} />}
          </Container>
        </>
        <Footer  />
      </>
    </ClientOnly>
  );
};

export default LandingPage;
