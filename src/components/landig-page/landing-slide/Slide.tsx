import { Row, Col } from "react-grid-system";
import style from "./slide.module.scss";
import Image, { StaticImageData } from "next/image";

type SlideData = {
  id: string;
  section: string;
  title: string[];
  speech: string[];
  illu: StaticImageData;
  background: StaticImageData;
  hidden?: boolean;
};

const Slide = (props: SlideData) => {
  return (
    <section id={props.id}>
      <Row className={style[props.section]}>
        {props.section === "white__section" ? (
          !props.hidden && <>
            <Col className={style.left} sm={12} lg={5}>
              <div className={style["white__section-visual"]}>
                <Image
                  style={{'zIndex': 1}}
                  className={"animate__animated animate__fadeIn animate__delay-03s"}
                  src={props.illu}
                  alt="illustration-ant"
                  placeholder="blur"
                  priority
                />
                  <Image
                  className={style["bg__block-1"] +" animate__animated animate__bounceIn"}
                  src={props.background}
                  alt="illustration-ant"
                  placeholder="blur"
                  priority
                />
              </div>
            </Col>
            <Col className={style.right} sm={12} lg={6}>
              <div
                className={
                  style["white__section-text"] +
                  " animate__animated animate__fadeInRight"
                }
              >
                <h1 className={style.entete}>
                  {props.title[0]}
                  <br />
                  {props.title[2] !== "" && props.title[1]}
                  <span className={style.highlight}> {props.title[2]}</span>
                </h1>
                <p className={style.story}>
                  {props.speech[0]}
                  <br />
                  <br />
                  {props.speech[1]}
                </p>
              </div>
            </Col>
          </>
        ) : (
          !props.hidden && <>
          <Col className={style.left} sm={12} offset={{ lg: 1 }} lg={6}>
            <div className={style["grey__section-text"] + " animate__animated animate__fadeInLeft"}>
              <h1 style={{ maxWidth: "490px" }} className={style.entete}>
                {props.title[0]}{" "}
                <span className={style.highlight}>{props.title[1]}</span>{" "}
                {props.title[2]}
              </h1>
              <p className={style.story}>
                {props.speech[0]}
                <br />
                {props.speech[1]}
              </p>
            </div>
          </Col>
          <Col className={style.right} sm={12} lg={5}>
            <div className={style["grey__section-visual"]}>
            <Image
                style={{'zIndex': 1}}
                className={"animate__animated animate__fadeIn animate__delay-03s"}
                src={props.illu}
                alt="illustration-ant"
                placeholder="blur"
                priority
              />
              <Image
                className={style["bg__block-2"] +" animate__animated animate__bounceIn"}
                src={props.background}
                alt="illustration-ant"
                placeholder="blur"
                priority
              />
            </div>
          </Col>
        </>
        )}
      </Row>
    </section>
  );
};

export default Slide;
