import style from "./bandeau.module.scss";
import { Row, Col } from "react-grid-system";
import Item from "./Item";

type BandeauData = {
  items: any[];
};

const Bandeau = (props: BandeauData) => {
  return (
    <Row>
      <Col
        className={
          style["bandeau-container"] + " animate__animated animate__fadeIn"
        }
        sm={12}
      >
        <div className={style.bandeau}>
          <Col
            sm={12}
            lg={12}
            xxl={10}
            offset={{ sm: 0, md: 0, xxl: 1 }}
            className={style["bandeau-stats-container"]}
          >
            <Row style={{ height: "100%" }}>
              {props.items.map((item, index) => {
                return (
                  <Col
                    key={`bandeau-${index}`}
                    xs={6}
                    md={3}
                    lg={3}
                    xl={3}
                    xxl={3}
                    className={
                      style.stat +
                      " animate__animated animate__bounceIn animate__delay-03s"
                    }
                  >
                    <Item
                      stat={item.stat}
                      title={item.title}
                      illu={item.illu}
                    />
                  </Col>
                );
              })}
            </Row>
          </Col>
        </div>
      </Col>
    </Row>
  );
};

export default Bandeau;
