import Image, { StaticImageData } from "next/image";
import style from "./bandeau.module.scss";
import { useState } from "react";

type ItemData = {
  stat: string;
  title: string;
  illu: StaticImageData[];
};

const Item = (props: ItemData) => {
  const [isHovering, setIsHovering] = useState(false);

  return (
    <div className={style["bandeau-stats-container"]}>
      <div 
      onMouseOver={() => setIsHovering(true)}
      onMouseOut={() => setIsHovering(false)}
      className={style.stat}>
        <Image
          style={{ zIndex: 1 }}
          className={
            isHovering
              ? "animate__animated animate__tada animate__infinite"
              : "animate__animated animate__fadeIn"
          }
          src={!isHovering ? props.illu[0] : props.illu[1]}
          alt="bandeau-icon"
          priority
          height={100}
          width={100}
        />
        <span className={style["text-stat"]}>{props.stat}</span>
        <span className={style["text-title"]}>{props.title}</span>
      </div>
    </div>
  );
};

export default Item;