import style from "./menu.module.scss";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import logo from "../../../../public/assets/img/logo.png";
import Item from "./Item";
import UserWidget from "../user-widget/UserWidget";
import SwitchLang from "../banner/i18n-switcher/SwitchLang";
import router from "next/router";
import ClientOnly from "../../ClientOnly";
import CoinWidget from "@/components/myrmecoins/coinWidget/CoinWidget";

const Menu = ({ tr }: { tr: any }) => {
  const { t } = useTranslation(["menu"]);
  const menuItems = [
    t("HOME"),
    t("MY_NESTS"),
    t("BREEDING_SHEETS"),
    t("BLOGS"),
    t("ARTICLES"),
    t("OFFERS"),
    t("SHOPS"),
  ];

  return (
    <ClientOnly>
      <>
        <div className={style["menu-container"]}>
          <button onClick={() => router.push("/")}>
            <div
              style={{ position: "relative", height: "60px", width: "60px" }}
            >
              <Image
                className={style.logo + " animate__animated animate__flipInY"}
                width={60}
                height={60}
                src={logo}
                alt="logo"
                placeholder="blur"
                priority
              />
            </div>
          </button>
          <div className={style.menu}>
            {menuItems.map((item, index) => {
              return (
                <div key={index}>
                  <Item text={item} />
                </div>
              );
            })}
          </div>

          <div className={style["header-modules"]}>
            <CoinWidget />
            <UserWidget />
            <SwitchLang theme="black" />
          </div>
        </div>
      </>
    </ClientOnly>
  );
};

export default Menu;
