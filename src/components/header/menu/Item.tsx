import { useState } from "react";
import style from "./menu.module.scss";

type ItemProps = {
  text: string;
}

const Item = (props: ItemProps) => {
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseOver = () => {
    setIsHovered(true);
  };

  const handleMouseOut = () => {
    setIsHovered(false);
  };

  return (
    <span 
    onMouseOver={() => handleMouseOver()}
    onMouseOut={() => handleMouseOut()}
    className={isHovered ? style["menu-item"] + " " + style["menu-item-highlight"] : style["menu-item"]}>
      {props.text}
    </span>
  );
};

export default Item;