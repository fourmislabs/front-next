import Link from "next/link";
import styles from "./banner.module.scss";
import { useMediaQuery } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import router from "next/router";
import SwitchLang from "./i18n-switcher/SwitchLang";
import ClientOnly from "@/components/ClientOnly";

const Banner = ({ tr }: { tr: any }) => {
  const { t } = useTranslation("banner");
  const [matches] = useMediaQuery("(max-width: 576px)", { ssr: true });

  return (
    <ClientOnly>
      <div className={styles.banner}>
        <SwitchLang theme="white" />
        <Link aria-label="Read more about Seminole tax hike" href="/">
          <i className={styles.logo}></i>
        </Link>
        <div className={styles.overlay} />
        <header className={styles.content}>
          <h1>
            fourmis<span>labs</span>
          </h1>
          <span className={styles.baseline}>{t("BASELINE")}</span>
          <div className={styles.buttons}>
            <button
              onClick={() => router.push(`/auth?signup=true`)}
              className={"btn-white btn"}
            >
              {t("SIGNUP")}
            </button>

            <button
              className={"btn-green btn"}
              onClick={() => router.push(`/auth?login=true`)}
            >
              {t("LOGIN")}
            </button>
          </div>
        </header>

        {!matches ? (
          <div
            style={{
              minHeight: "485px",
              minWidth: "100%",
            }}
          >
            <video id="banner" autoPlay loop muted>
              <source
                src="../assets/img/header/banner.webm"
                type="video/webm"
              />
            </video>
          </div>
        ) : (
          <div
            style={{
              minHeight: "190px",
              minWidth: "100%",
            }}
          >
            <video id="banner-mobile" autoPlay loop muted>
              <source
                src="./assets/img/header/banner-mobile.webm"
                type="video/webm"
              />
            </video>
          </div>
        )}
      </div>
    </ClientOnly>
  );
};

export default Banner;
