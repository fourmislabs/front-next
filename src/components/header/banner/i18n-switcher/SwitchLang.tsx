"use client"
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import style from "./switch-lang.module.scss";
import { useTranslation } from "next-i18next";
import { DEFAULT_LANGUAGE, languages } from "../../../../utils/enums/i18n.enum";
import { useOutsideClick } from "@chakra-ui/react";

type ThemeProp = {
  theme: string;
};

const SwitchLang = (props: ThemeProp) => {
  const router = useRouter();
  const { i18n } = useTranslation();
  const [optionsOpen, setOptionsOpen] = useState<boolean>(false);
  const ref = useRef(null);

  useEffect(() => {
    if(localStorage.getItem('i18nextLng') && localStorage.getItem('i18nextLng') !== i18n.language)
    {
      i18n.changeLanguage(localStorage.getItem('i18nextLng')!)
    } 
  }, [i18n.language,i18n]);

  const onToggleLanguageClick = (newLocale: string) => {
    const { pathname, asPath, query } = router;
    localStorage.setItem("i18nextLng", newLocale);
    router.push({ pathname, query }, asPath, { locale: newLocale });
  };

  const changeTo = router.locale === "fr" ? "en" : "fr";

  const handleClickOutside = () => {
    setOptionsOpen(false);
  };

  useOutsideClick({
    ref: ref,
    handler: handleClickOutside,
  });

  return (
    <div
      onClick={() => setOptionsOpen(!optionsOpen)}
      className={
        props.theme === "white"
          ? style["switch-lang"] + " " + style.white
          : style["switch-lang"] + " " + style.black
      }
    >
      <span>{i18n.language}</span>
      {optionsOpen && (
        <div
          ref={ref}
          className={style.options + " animate__animated animate__bounceIn"}
        >
          {languages.map((lang, i) => {
            return (
              <span
                style={
                  props.theme === "white"
                    ? { color: "white" }
                    : { color: "black" }
                }
                className={`animate__animated animate__fadeIn animate__delay-0${
                  i + 1
                }s`}
                key={i}
                onClick={() => {
                  if (lang !== i18n.language) {
                    onToggleLanguageClick(changeTo);
                  }
                }}
              >
                {lang}
              </span>
            );
          })}
        </div>
      )}
      <i
        className={
          props.theme === "white"
            ? "icon caret-select-down-white"
            : "icon caret-select-down-black"
        }
      ></i>
    </div>
  );
};

export default SwitchLang;
