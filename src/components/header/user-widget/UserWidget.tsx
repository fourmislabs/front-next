"use client"
import style from "./user-widget.module.scss";
import { useUserStore } from "@/stores/userStore";
import { useState, useCallback, useEffect } from "react";
import { Avatar, Link } from "@chakra-ui/react";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";

const UserWidget = () => {
  const { i18n } = useTranslation();
  const router = useRouter();
  const [Lng, setLng] = useState(i18n.language);

  useEffect(() => {
    setLng(i18n.language);
    console.log("here")
  }, [i18n.language]);

  const [fr] = useState({
    admin: "Admin",
    moderator: "Modérateur",
    logout: "Déconnexion",
    profileSettings: "Paramètres",
    profile: "Profil",
  });
  const [en] = useState({
    admin: "Admin",
    moderator: "Moderator",
    logout: "Logout",
    profileSettings: "Profile settings",
    profile: "Profile",
  });

  const [isAvatarHovered, setIsAvatarHovered] = useState(false);
  const [isMenuHovered, setIsMenuHovered] = useState(false);
  const [userId, avatar, username, isAdmin, isModerator, initStore] =
    useUserStore((state) => [
      state.userId,
      state.avatar,
      state.username,
      state.isAdmin,
      state.isModerator,
      state.initStore,
    ]);

  const handleLogout = useCallback(() => {
    fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/auth/logout`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "user-id": userId,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          setIsMenuHovered(false);
          setIsAvatarHovered(false);
          initStore()
          window.location.href =`${process.env.NEXT_PUBLIC_BASE_URL}/auth?login=true`
        }
      })
      .catch((err) => {
        initStore();
        setIsMenuHovered(false);
        setIsAvatarHovered(false);
        router.push("/");
      });
  }, [router, userId, initStore]);

  const onAvatarMouseOver = useCallback(() => {
    setIsAvatarHovered(true);
  }, []);

  const onAvatarMouseOut = useCallback(() => {
    if (!isMenuHovered) setIsAvatarHovered(false);
  }, [isMenuHovered]);

  return (
    <div
      onMouseOver={onAvatarMouseOver}
      onMouseOut={onAvatarMouseOut}
      className={
        isAvatarHovered
          ? style["user-widget"] +
            " animate__animated animate__faster animate__blink"
          : style["user-widget"]
      }
    >
      {}
      <Avatar size="md" bg={"green.400"} name={username} src={avatar}
       referrerPolicy="no-referrer"
      />
      {isAvatarHovered && (
        <div
          className={
            isAvatarHovered
              ? style["user-widget-menu"] +
                " animate__animated animate__faster animate__zoomInUp"
              : style["user-widget-menu"] +
                " animate__animated animate__faster animate__zoomInUp"
          }
        >
          {isAdmin && (
            <Link
              className="admin"
              href="/admin"
              onClick={() => {
                setIsMenuHovered(false);
                setIsAvatarHovered(false);
              }}
            >
              <i className="icon user-menu-admin" />
              {Lng === "fr" ? fr.admin : en.admin}
            </Link>
          )}
          {isModerator && (
            <Link
              className="moderator"
              href="/moderator"
              onClick={() => {
                setIsMenuHovered(false);
                setIsAvatarHovered(false);
              }}
            >
              <i className="icon user-menu-modo" />
              {Lng === "fr" ? fr.moderator : en.moderator}
            </Link>
          )}
          <Link
            className="link"
            onClick={() => {
              setIsMenuHovered(false);
              setIsAvatarHovered(false);
              router.push("/profile?general=true");
            }}
          >
            <i className="icon user-menu-profile" />
            {Lng === "fr" ? fr.profile : en.profile}
          </Link>
          <div className={style.separator} />
          <Link
            className="link"
            onClick={() => {
              setIsMenuHovered(false);
              setIsAvatarHovered(false);
              router.push("/profilesettings");
            }}
          >
            <i className="icon user-menu-gear" />
            {Lng === "fr" ? fr.profileSettings : en.profileSettings}
          </Link>
          <Link
            onClick={() => {
              handleLogout();
            }}
          >
            <i className="icon user-menu-logout" />
            {Lng === "fr" ? fr.logout : en.logout}
          </Link>
        </div>
      )}
    </div>
  );
};

export default UserWidget;
