import style from './loader.module.scss';
import { useTranslation } from "next-i18next";
import loadingAnim from "../../../public/assets/img/loading.gif";
import Image from "next/image";

const Loader = () => {
  const { t } = useTranslation(["common"]);

  return (
    <div className={style["loader-container"]}>
    <Image
    src={loadingAnim}
    alt="loading"
    priority
  />
      <p className={style["loading-text"]}>{t("LOADING")}</p>
    </div>
  );
};

export default Loader;