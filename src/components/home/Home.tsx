import dynamic from "next/dist/shared/lib/dynamic";
import ClientOnly from "../ClientOnly";

const Footer = dynamic(() => import("../footer/Footer"), {
  loading: () => <></>,
});

const Home = ({ tr }: { tr: any }) => {
  return (
    <ClientOnly>
      <>
        <div>Homepage</div>
      </>
      <Footer />
    </ClientOnly>
  );
};

export default Home;
