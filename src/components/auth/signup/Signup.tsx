import { useCallback, useEffect, useState } from "react";
import style from "../../../styles/pages/auth-page.module.scss";
import { useTranslation } from "next-i18next";
import dynamic from "next/dynamic";

const Footer = dynamic(() => import("../../footer/Footer"));

import {
  validatePasword,
  validateUsername,
  validateEmail,
} from "../../../utils/form.validator";

import { GoogleOAuthProvider, GoogleLogin } from "@react-oauth/google";
import { useRouter } from "next/router";
import { Box, useToast } from "@chakra-ui/react";
import { useUserStore } from "@/stores/userStore";

const SignUp = (props: any) => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState(false);
  const [usernameError, setUsernameError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);
  const [
    setUserId,
    setUserName,
    setmail,
    setAvatar,
    setIsAdmin,
    setIsModerator,
    setIsConnected,
    setProfileId,
  ] = useUserStore((state) => [
    state.setUserId,
    state.setUsername,
    state.setEmail,
    state.setAvatar,
    state.setIsAdmin,
    state.SetIsModerator,
    state.setIsConnected,
    state.setProfileId,
  ]);
  const router = useRouter();

  const { t } = useTranslation(["auth-page"]);
  const toast = useToast();

  const closeToasts = useCallback(() => {
    toast.closeAll();
  }, [toast]);

  const handleSubmit = useCallback(
    async (e: React.SyntheticEvent): Promise<void> => {
      e.preventDefault();
      fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/auth/signup`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ username, email, password }),
      }).then((res) => {
        if (res.status === 201) {
          const message = t("auth-page:SUCCESS.SIGNUP")!;
          toast({
            position: "bottom-right",
            description: message,
            duration: 3000,
            isClosable: true,
            render: () => (
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                borderRadius="8px"
                color="white"
                p={3}
                bg="green.400"
              >
                {message}
                <i
                  className={"icon cross-white"}
                  onClick={() => closeToasts()}
                />
              </Box>
            ),
          });
          setTimeout(() => {
            router.push("/auth?login=true");
          }, 3009);
        } else if (res.status === 409) {
          const message = t("auth-page:ERRORS.SIGNUP_401")!;
          toast({
            position: "bottom-right",
            description: message,
            duration: 3000,
            isClosable: true,
            render: () => (
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                borderRadius="8px"
                color="white"
                p={3}
                bg="red.400"
              >
                {message}
                <i
                  className={"icon cross-white"}
                  onClick={() => closeToasts()}
                />
              </Box>
            ),
          });
        }
        else if (res.status===400){
          const message = t("auth-page:ERRORS.400")!;
          toast({
            position: "bottom-right",
            description: message,
            duration: 3000,
            isClosable: true,
            render: () => (
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                borderRadius="8px"
                color="white"
                p={3}
                bg="red.400"
              >
                {message}
                <i
                  className={"icon cross-white"}
                  onClick={() => closeToasts()}
                />
              </Box>
            ),
          });

        }
      });
    },
    [username, email, password, t, closeToasts, toast, router]
  );

  const verifyAndSetUsername = useCallback(
    (e: React.FormEvent<HTMLInputElement>): void => {
      if (!validateUsername(e.currentTarget.value)) {
        setUsernameError(true);
      } else {
        setUsernameError(false);
      }
      setUsername(e.currentTarget.value);
    },
    [setUsernameError, setUsername]
  );

  const verifyAndSetEmail = useCallback(
    (e: React.FormEvent<HTMLInputElement>): void => {
      if (!validateEmail(e.currentTarget.value)) {
        setEmailError(true);
      } else {
        setEmailError(false);
      }
      setEmail(e.currentTarget.value);
    },
    [setEmailError, setEmail]
  );

  const verifyAndSetPassword = useCallback(
    (e: React.FormEvent<HTMLInputElement>): void => {
      if (!validatePasword(e.currentTarget.value)) {
        setPasswordError(true);
      } else {
        setPasswordError(false);
      }
      setPassword(e.currentTarget.value);
    },
    [setPasswordError, setPassword]
  );

  useEffect(() => {
    if (usernameError || emailError || passwordError) setIsDisabled(true);
    else setIsDisabled(false);
  }, [passwordError, emailError, usernameError]);
  return (
    <>
      <div className={style["auth-form"]}>
        <div className={style.logo}>
          <div
            className={
              "animate__animated animate__rubberBand" + " " + style["bg-circle"]
            }
          />
        </div>
        <h5 className={style.title}>{t("auth-page:REGISTER")}</h5>
        <form onSubmit={handleSubmit}>
          <input
            className={style.input}
            type="text"
            name="username"
            value={username}
            placeholder={t("auth-page:USERNAME") as string}
            onChange={verifyAndSetUsername}
            required={true}
          />
          {usernameError && (
            <span className="error-message animate__animated animate__fadeInUp">
              {t("auth-page:ERRORS.USERNAME")}
            </span>
          )}
          <input
            className={style.input}
            type="text"
            name="email"
            value={email}
            placeholder={t("auth-page:EMAIL") as string}
            onChange={verifyAndSetEmail}
            required={true}
          />
          {emailError && (
            <span className="error-message animate__animated animate__fadeInUp">
              {t("auth-page:ERRORS.EMAIL")}
            </span>
          )}
          <input
            className={style.input}
            type="password"
            name="password"
            value={password}
            placeholder={t("auth-page:PASSWORD") as string}
            onChange={verifyAndSetPassword}
            required={true}
          />
          {passwordError && (
            <span className="error-message animate__animated animate__fadeInUp">
              {t("auth-page:ERRORS.PASSWORD")}
            </span>
          )}
          <button
            disabled={isDisabled}
            className={
              isDisabled ? `btn btn-disabled btn-green` : "btn btn-green"
            }
            type="submit"
            value="Signup"
          >
            {t("auth-page:CREATE_ACCOUNT")}
          </button>
        </form>
        <div className={style.separator} />
        <span className={style.social}>{t("SOCIAL_CONNECT")}</span>
        <div className={style["social-auth"]}>
          <GoogleOAuthProvider
            clientId={process.env.NEXT_PUBLIC_GOOGLE_AUTH_CLIENT_ID!}
          >
            <GoogleLogin
              type="icon"
              locale={router.locale}
              size="large"
              width="300"
              onSuccess={async (credentiels) => {
                await fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/auth/google`, {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json",
                  },
                  body: JSON.stringify({
                    token: credentiels.credential,
                  }),
                }).then(async (res) => {
                  if (res.status === 200) {
                    const datas = await res.json();
                    setUserId(datas.user._id);
                    setUserName(datas.user.username);
                    setmail(datas.user.email);
                    setAvatar(datas.user.avatar);
                    setIsAdmin(false);
                    setIsModerator(false);
                    setIsConnected(true);
                    setProfileId(datas.user.profileId);
                  }
                  router.push("/");
                });
              }}
              onError={() => {
                toast({
                  position: "bottom-right",
                  description: t("auth-page:ERRORS.GOOGLE"),
                  duration: 3000,
                  isClosable: true,
                  render: () => (
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      alignItems="center"
                      borderRadius="8px"
                      color="white"
                      p={3}
                      bg="red.400"
                    >
                      {t("auth-page:ERRORS.GOOGLE")}
                      <i
                        className={"icon cross-white"}
                        onClick={() => closeToasts()}
                      />
                    </Box>
                  ),
                });
              }}
            />
          </GoogleOAuthProvider>

          <button
            className={style["discord-btn"]}
            onClick={() =>
              router.push(`${process.env.NEXT_PUBLIC_DISCORD_AUTH_LINK}`)
            }
          >
            <i className="icon discord" />
          </button>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default SignUp;
