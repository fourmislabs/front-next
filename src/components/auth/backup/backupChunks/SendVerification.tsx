import { validateEmail } from "@/utils/form.validator";
import { Box, useToast } from "@chakra-ui/react";
import { useRef, useCallback } from "react";
import { useTranslation } from "next-i18next";
import style from "./backup.module.scss";
import { Row, Col } from "react-grid-system";

const SendVerification = () => {
  const { t } = useTranslation("reset-password");
  const email = useRef<HTMLInputElement>(null);
  const toast = useToast();

  const closeToasts = useCallback(() => {
    toast.closeAll();
  }, [toast]);

  const sendBackupEmail = useCallback(
    async (e: any) => {
      e.preventDefault();
      console.log(email.current?.value!);
      if (validateEmail(email.current?.value!)) {
        const response = await fetch(
          `${process.env.NEXT_PUBLIC_BASE_URL}/api/users/auth/forgotPassword`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(email.current?.value!),
          }
        );

        if (response.ok) {
          toast({
            position: "bottom-right",
            description: "success",
            duration: 3000,
            isClosable: true,
            render: () => (
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                borderRadius="8px"
                color="white"
                p={3}
                bg="green.400"
              >
                {t("RESET_EMAIL_SUCCESS")}
                <i
                  className={"icon cross-white"}
                  onClick={() => closeToasts()}
                />
              </Box>
            ),
          });
        } else {
          toast({
            position: "bottom-right",
            description: "error",
            duration: 3000,
            isClosable: true,
            render: () => (
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                borderRadius="8px"
                color="white"
                p={3}
                bg="red.400"
              >
                {t("ERRORS.ENTER_VALID_EMAIL")}
                <i
                  className={"icon cross-white"}
                  onClick={() => closeToasts()}
                />
              </Box>
            ),
          });
        }
      }
    },
    [closeToasts, t, toast]
  );

  return (
    <Row>
      <Col
        sm={12}
        lg={6}
        offset={{ lg: 3 }}
        className={style["send-verification"]}
      >
        <div className={style.logo}>
          <div
            className={
              "animate__animated animate__rubberBand" + " " + style["bg-circle"]
            }
          />
        </div>
        <h5 className={style.title}>{t("FORGOT_PASSWORD")}</h5>
        <form>
          <input
            placeholder={t("ENTER_EMAIL") as string}
            ref={email}
            className={style.input}
          />
          <button className={"btn btn-green"} onClick={sendBackupEmail}>
            {t("SEND_EMAIL")}
          </button>
        </form>
      </Col>
    </Row>
  );
};

export default SendVerification;
