import { validatePasword } from "@/utils/form.validator";
import { Box, useToast } from "@chakra-ui/react";
import router from "next/router";
import { useCallback, useRef } from "react";
import { useTranslation } from "next-i18next";
import style from "./backup.module.scss";
import { Row, Col } from "react-grid-system";

const ResetPasswordForm = () => {
  const { t } = useTranslation("reset-password");
  const toast = useToast();
  const passwordInput = useRef<HTMLInputElement>(null);
  const confirmPasswordInput = useRef<HTMLInputElement>(null);
  const verifyAndSetPassword = useCallback(async () => {
    if (
      !validatePasword(passwordInput.current?.value!) ||
      passwordInput.current?.value !== confirmPasswordInput.current?.value!
    ) {
      toast({
        position: "bottom-right",
        description: "error",
        duration: 3000,
        isClosable: true,
        render: () => (
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            borderRadius="8px"
            color="white"
            p={3}
            bg="red.400"
          >
            {t("ERRORS.WEAK_PASSWORD")}
          </Box>
        ),
      });
    } else if (
      validatePasword(passwordInput.current?.value!) &&
      passwordInput.current?.value === confirmPasswordInput.current?.value!
    ) {
      const res = await fetch(
        `${process.env.NEXT_PUBLIC_BASE_URL}/api/users/auth/forgotPassword?key=${router.query.key}`,
        {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(passwordInput.current?.value!),
        }
      );
      if (res.ok) {
        toast({
          position: "bottom-right",
          description: "success",
          duration: 3000,
          isClosable: true,
          render: () => (
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              borderRadius="8px"
              color="white"
              p={3}
              bg="green.400"
            >
              {t("RESET_PASSWORD_SUCCESS")}
            </Box>
          ),
        });
      } else if (!res.ok) {
        toast({
          position: "bottom-right",
          description: "error",
          duration: 3000,
          isClosable: true,
          render: () => (
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              borderRadius="8px"
              color="white"
              p={3}
              bg="red.400"
            >
              {t("EMAIL_NOT_FOUND")}
            </Box>
          ),
        });
      }
    }
  }, [toast, t]);

  return (
    <Row>
      <Col sm={12} lg={6} offset={{ lg: 3 }} className={style["reset-pwd"]}>
        <div className={style.logo}>
          <div
            className={
              "animate__animated animate__rubberBand" + " " + style["bg-circle"]
            }
          />
        </div>
        <h5 style={{ marginBottom: "15px" }} className={style.title}>
          {t("RESET_PASSWORD")}
        </h5>

        <input
          className={style["input"]}
          type="password"
          name="password"
          placeholder={t("PASSWORD") as string}
          ref={passwordInput}
        />
        <input
          className={style.input}
          type="password"
          name="confirmPassword"
          placeholder={t("CONFIRM_PASSWORD") as string}
          ref={confirmPasswordInput}
        />
        <button
          style={{ marginTop: "15px", marginBottom: "20px"  }}
          className={"btn btn-green"}
          onClick={verifyAndSetPassword}
        >
          {t("SUBMIT")}
        </button>
      </Col>
    </Row>
  );
};

export default ResetPasswordForm;
