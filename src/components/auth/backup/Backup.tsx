import { useCallback, useEffect, useState } from "react";
import ResetPasswordForm from "@/components/auth/backup/backupChunks/ResetPasswordForm";
import SendVerification from "@/components/auth/backup/backupChunks/SendVerification";
import router from "next/router";
import { useTranslation } from "react-i18next";
import style from "./backupChunks/backup.module.scss";
import { Row, Col } from "react-grid-system";
import dynamic from "next/dynamic";

const Footer = dynamic(() => import("../../footer/Footer"));

const Backup = (props: any) => {
  const { t } = useTranslation(["reset-password"]);
  const [isJwtExpired, setIsJwtExpired] = useState(false);
  const [isJwt, setIsJwt] = useState(false);

  const parseToken = useCallback((token: string) => {
    if (token !== null && token !== undefined && token !== "") {
      var base64Url = token.split(".")[1];
      if (!base64Url) {
        return null;
      } else {
        var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
        var jsonPayload = decodeURIComponent(
          window
            .atob(base64)
            .split("")
            .map(function (c) {
              return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
            })
            .join("")
        );

        return JSON.parse(jsonPayload);
      }
    }
  }, []);

  useEffect(() => {
    const jwt = parseToken(router.query.key as string);
    if (!jwt) {
      setIsJwt(false);
    } else if (jwt) {
      setIsJwt(true);
      let currentTimestamp = new Date().getTime() / 1000;
      let tokenIsExpired = jwt.exp > currentTimestamp;
      setIsJwtExpired(tokenIsExpired);
    }
  }, [parseToken]);

  return (
    <div>
      {isJwt ? (
        isJwtExpired ? (
          <ResetPasswordForm />
        ) : (
          <>
            <Row>
              <Col
                sm={12}
                lg={6}
                offset={{ lg: 3 }}
                className={style["backup"]}
              >
                <div className={style.logo}>
                  <div
                    className={
                      "animate__animated animate__rubberBand" +
                      " " +
                      style["bg-circle"]
                    }
                  />
                </div>
                <span className={style["error-token"]}>
                  {t("ERRORS.BACKUP_ERROR")}
                </span>
                <button
                  className={"btn btn-green"}
                  onClick={() => router.push("/auth/login")}
                >
                  {t("GO_BACK")}
                </button>
              </Col>
            </Row>
          </>
        )
      ) : (
        <SendVerification />
      )}
    <Footer />
    </div>
  );
};

export default Backup;
