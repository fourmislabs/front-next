"use client";
import { useCallback, useEffect, useState } from "react";
import { validatePasword, validateEmail } from "../../../utils/form.validator";
import style from "../../../styles/pages/auth-page.module.scss";
import { useUserStore } from "@/stores/userStore";
import { useTranslation } from "next-i18next";
import { GoogleOAuthProvider, GoogleLogin } from "@react-oauth/google";
import { useRouter } from "next/router";
import { Box, useToast, useUpdateEffect } from "@chakra-ui/react";
import dynamic from "next/dynamic";
import { ERoleName } from "@/utils/enums/roles.enum";

const Footer = dynamic(() => import("../../footer/Footer"));

const Login = ({ tr }: { tr: any }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);
  const [
    setUserId,
    setUserName,
    setmail,
    setAvatar,
    setIsAdmin,
    setIsModerator,
    setIsConnected,
    setProfileId,
  ] = useUserStore((state) => [
    state.setUserId,
    state.setUsername,
    state.setEmail,
    state.setAvatar,
    state.setIsAdmin,
    state.SetIsModerator,
    state.setIsConnected,
    state.setProfileId,
  ]);
  const [discordCode, setDiscordCode] = useState("");
  const router = useRouter();

  const { t } = useTranslation(["auth-page"]);
  const toast = useToast();

  const closeToasts = useCallback(() => {
    toast.closeAll();
  }, [toast]);

  useEffect(() => {
    if (router.query.code) {
      setDiscordCode(router.query.code as string);
    }
  }, [router.query.code]);

  const handleSubmit = useCallback(
    (e: React.SyntheticEvent) => {
      e.preventDefault();
      fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/auth/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email, password }),
      })
        .then(async (res) => {
          if (res.ok) {
            setIsConnected(true);
            return await res
              .json()

              .then((datas) => {
                setUserId(datas.user._id);
                setUserName(datas.user.username);
                setmail(datas.user.email);
                setAvatar(datas.user.avatar);
                setProfileId(datas.user.profileId);
                if (datas.user.roles.indexOf(ERoleName.MODERATOR) !== -1) {
                  setIsModerator(true);
                }
                if (datas.user.roles.indexOf(ERoleName.ADMIN) !== -1) {
                  setIsAdmin(true);
                }
              })
              .then(() => router.push("/"));
          } else if (res.status === 401) {
            const message = t("auth-page:ERRORS.LOGIN_401")!;
            toast({
              position: "bottom-right",
              description: message,
              duration: 3000,
              isClosable: true,
              render: () => (
                <Box
                  display="flex"
                  justifyContent="space-between"
                  alignItems="center"
                  borderRadius="8px"
                  color="white"
                  p={3}
                  bg="red.400"
                >
                  {message}
                  <i
                    className={"icon cross-white"}
                    onClick={() => closeToasts()}
                  />
                </Box>
              ),
            });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    },
    [
      closeToasts,
      email,
      password,
      t,
      toast,
      router,
      setAvatar,
      setIsAdmin,
      setIsModerator,
      setIsConnected,
      setmail,
      setProfileId,
      setUserId,
      setUserName,
    ]
  );

  const verifyAndSetEmail = useCallback(
    (e: React.FormEvent<HTMLInputElement>): void => {
      if (!validateEmail(e.currentTarget.value)) {
        setEmailError(true);
      } else {
        setEmailError(false);
      }
      setEmail(e.currentTarget.value);
    },
    [setEmailError, setEmail]
  );

  const verifyAndSetPassword = useCallback(
    (e: React.FormEvent<HTMLInputElement>): void => {
      if (!validatePasword(e.currentTarget.value)) {
        setPasswordError(true);
      } else {
        setPasswordError(false);
      }
      setPassword(e.currentTarget.value);
    },
    [setPasswordError, setPassword]
  );

  useEffect(() => {
    if (emailError || passwordError) setIsDisabled(true);
    else setIsDisabled(false);
  }, [passwordError, emailError]);

  const discordHandler = useCallback(() => {
    fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/auth/discord`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ code: discordCode }),
    })
      .then(async (res) => {
        if (res.status === 200) {
          const datas = await res.json();
          setUserId(datas.user._id);
          setUserName(datas.user.username);
          setmail(datas.user.email);
          setAvatar(datas.user.avatar);
          setIsConnected(true);
          setProfileId(datas.user.profileId);
          if (datas.user.roles.indexOf(ERoleName.MODERATOR) !== -1) {
            setIsModerator(true);
          }
          if (datas.user.roles.indexOf(ERoleName.ADMIN) !== -1) {
            setIsAdmin(true);
          }

          router.push("/");
        } else if (res.status === 401) {
          const datas = await res.json();
          toast({
            position: "bottom-right",
            description: datas.message,
            duration: 3000,
            isClosable: true,
            render: () => (
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                borderRadius="8px"
                color="white"
                p={3}
                bg="red.400"
              >
                {datas.message}
                <i
                  className={"icon cross-white"}
                  onClick={() => closeToasts()}
                />
              </Box>
            ),
          });
          setDiscordCode("");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, [
    discordCode,
    setUserId,
    setUserName,
    setmail,
    setAvatar,
    setIsAdmin,
    setIsModerator,
    setIsConnected,
    setProfileId,
    router,
    toast,
    closeToasts,
  ]);

  useUpdateEffect(() => {
    if (discordCode?.length) {
      discordHandler();
    }
  }, [discordCode]);

  return (
    <>
      <div className={style["auth-form"]}>
        <div className={style.logo}>
          <div
            className={
              "animate__animated animate__rubberBand" + " " + style["bg-circle"]
            }
          />
        </div>
        <h5 className={style.title}>{t("auth-page:CONNECT")}</h5>
        <form onSubmit={handleSubmit}>
          <input
            className={style.input}
            type="text"
            name="email"
            value={email}
            placeholder={t("auth-page:EMAIL") as string}
            onChange={verifyAndSetEmail}
            required={true}
          />
          {emailError && (
            <span className="error-message animate__animated animate__fadeInUp">
              {t("auth-page:ERRORS.EMAIL")}
            </span>
          )}
          <input
            className={style.input}
            type="password"
            name="password"
            value={password}
            placeholder={t("auth-page:PASSWORD") as string}
            onChange={verifyAndSetPassword}
            required={true}
          />
          {passwordError && (
            <span className="error-message animate__animated animate__fadeInUp">
              {t("auth-page:ERRORS.PASSWORD_INVALID")}
            </span>
          )}
          <button
            type="submit"
            disabled={isDisabled}
            className={
              isDisabled ? "btn btn-disabled btn-green" : "btn btn-green"
            }
          >
            {t("auth-page:LAUNCH_APP")}
          </button>
        </form>
        <span
          className={style["forgot-password"]}
          onClick={() => router.push("/auth?backup=true")}
        >
          {t("auth-page:FORGOT_PASSWORD_LINK")}
        </span>
        <div className={style.separator} />
        <span className={style.social}>{t("SOCIAL_CONNECT")}</span>
        <div className={style["social-auth"]}>
          <GoogleOAuthProvider
            clientId={process.env.NEXT_PUBLIC_GOOGLE_AUTH_CLIENT_ID!}
          >
            <GoogleLogin
              type="icon"
              locale={router.locale}
              size="large"
              width="300"
              onSuccess={async (credentiels) => {
                await fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/auth/google`, {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json",
                  },
                  body: JSON.stringify({
                    token: credentiels.credential,
                  }),
                }).then(async (res) => {
                  if (res.status === 200) {
                    const datas = await res.json();
                    console.log(datas);
                    setUserId(datas.user._id);
                    setUserName(datas.user.username);
                    setmail(datas.user.email);
                    setAvatar(datas.user.avatar);
                    setIsConnected(true);
                    setProfileId(datas.user.profileId);
                    if(datas.user.roles.indexOf(ERoleName.MODERATOR)!==-1){
                      setIsModerator(true)
                    }
                     if(datas.user.roles.indexOf(ERoleName.ADMIN)!==-1){
                         setIsAdmin(true)
                    }
                    ;
               
                  }
                  router.push("/");
                });
              }}
              onError={() => {
                toast({
                  position: "bottom-right",
                  description: t("auth-page:ERRORS.GOOGLE"),
                  duration: 3000,
                  isClosable: true,
                  render: () => (
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      alignItems="center"
                      borderRadius="8px"
                      color="white"
                      p={3}
                      bg="red.400"
                    >
                      {t("auth-page:ERRORS.GOOGLE")}
                      <i
                        className={"icon cross-white"}
                        onClick={() => closeToasts()}
                      />
                    </Box>
                  ),
                });
              }}
            />
          </GoogleOAuthProvider>

          <button
            className={style["discord-btn"]}
            onClick={() =>
              router.push(`${process.env.NEXT_PUBLIC_DISCORD_AUTH_LINK}`)
            }
          >
            <i className="icon discord" />
          </button>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Login;
